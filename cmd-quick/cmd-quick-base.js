/*******************************************************************************
Array-based collection functions

won't modify array
*******************************************************************************/
function find_if(arr, pred) {
    for (let i in arr) {
        if (pred(arr[i])) return i;
    }
    return -1;
}


function filter_if(arr, pred) {
    let filtered = [];
    for (let e of arr) {
        if (pred(e)) filtered.push(e);
    }
    return filtered;
}


function for_each(arr, func) {
    for (let e of arr) {
        func(e);
    }
}


function remove_if(arr, pred) {
    return filter_if(arr, function(e) {
        return !pred(e);
    });
}


function accumulate(arr, init, func) {
    let result = init;
    for (let e of arr) {
        result = func(e, result);
    }
    return result;
}


/*******************************************************************************
unique integer array
*******************************************************************************/
var UniqueIntArray = function() {
    this.array = [];

    this.Length = function() {
        return this.array.length;
    };

    this.Include = function(other) {
        for (let val of other.array) {
            if (this.array.indexOf(val) == -1) {
                return false;
            }
        }
        return true;
    };

    this.GetIndex = function(val) {
        return find_if(this.array, function(e) {
            return e == val;
        });
    };

    this.Add = function(val) {
        let index = this.GetIndex(val);
        if (index < 0) {
            this.array.push(val);
        }
    };

    this.Erase = function(val) {
        let index = this.GetIndex(val);
        if (index < 0) return;
        this.array.splice(index, 1);
    };

    this.Import = function(vals) {
        this.array = [];
        for (let val of vals) {
            this.array.push(val);
        }
    };

    this.Export = function() {
        return this.array;
    };

    this.Clone = function() {
        let n = new UniqueIntArray();
        for (let val of this.array) {
            n.array.push(val);
        }
        return n;
    };
};


function AllocateId(array) {
    let id = 0;
    if (array.length > 0) {
        id = array[array.length - 1].id;
        while (true) {
            ++id;
            if (find_if(array, function(e) {
                    return e.id == id;
                }) == -1)
                break;
        }
    }
    return id;
}


var Tag = function(id, name) {
    this.id = id;
    this.name = name;

    this.Update = function(other) {
        this.name = other.name;
    };

    this.Clone = function() {
        return new Tag(this.id, this.name);
    };
};


var TagArray = function() {
    this.array = [];

    this.GetIndexById = function(id) {
        return find_if(this.array, function(e) {
            return e.id == id;
        });
    };

    this.GetIndexByName = function(name) {
        return find_if(this.array, function(e) {
            return e.name === name;
        });
    };

    this.GetTagById = function(id) {
        let index = this.GetIndexById(id);
        if (index < 0) return null;
        return this.array[index];
    };

    this.GetTagByName = function(name) {
        let index = this.GetIndexByName(name);
        if (index < 0) return null;
        return this.array[index];
    };

    this.Contain = function(word) {
        return find_if(this.array, function(tag) {
            return tag.name.toLowerCase().indexOf(word.trim().toLowerCase()) != -1;
        }) != -1;
    };

    // return true if any word in words is in tags
    this.MatchAny = function(words) {
        for (let word of words) {
            if (word.length == 0) continue;
            if (this.Contain(word) != -1) return true;
        }
        return false;
    }


    // return true if everything in words are also in tags
    this.Include = function(other) {
        for (let tag of other.array) {
            if (this.Contain(tag.name) == false) {
                return false;
            }
        }
        return true;
    }


    // split text to tag, remove duplication
    this.FromText = function(text) {
        let parts = text.trim().toLowerCase().split(" ");
        let words = [];
        for (let word of parts) {
            if (word.length == 0) continue;
            if (words.indexOf(word) == -1) {
                words.push(word);
            }
        }
        this.array = [];
        for (let w of words) {
            this.Add(new Tag(-1, w));
        }
    };

    this.Add = function(newtag) {
        let tag = this.GetTagById(newtag.id);
        if (tag == null) {
            newtag.id = AllocateId(this.array);
            this.array.push(newtag);
            return newtag.id;
        } else {
            tag.Update(newtag);
            return tag.id;
        }
    };

    this.EraseById = function(id) {
        let index = this.GetIndexById(id);
        if (index < 0) return;
        this.array.splice(index, 1);
    };

    this.EraseByName = function(name) {
        let index = this.GetIndexByName(name);
        if (index < 0) return;
        this.array.splice(index, 1);
    };

    this.Import = function(tags) {
        this.array = [];
        for (let tag of tags) {
            this.array.push(new Tag(tag.id, tag.name));
        }
    };

    this.Export = function() {
        return this.array;
    };

    this.Clone = function() {
        let n = new TagArray();
        for (let tag of this.array) {
            n.array.push(tag.Clone());
        }
        return n;
    };
};


var Record = function(id, commands, detail, tagids) {
    this.id = id;
    this.commands = commands;
    this.detail = detail;
    this.tagids = tagids; // UniqueIntArray

    this.Update = function(other) {
        this.commands = other.commands;
        this.detail = other.detail;
        this.tagids = other.tagids;
    };

    this.ContainTag = function(tagid) {
        return this.tagids.GetIndex(tagid) != -1;
    };

    this.ToText = function() {
        let combined = this.commands.concat(this.detail);
        return combined.join("\n");
    };

    this.Import = function(other) {
        this.id = other.id;
        this.commands = other.commands;
        this.detail = other.detail;
        this.tagids.Import(other.tagids);
    };

    this.Export = function() {
        return {
            "id": this.id,
            "commands": this.commands,
            "detail": this.detail,
            "tagids": this.tagids.Export()
        };
    };

    this.Clone = function() {
        let nc = [];
        let nd = [];
        for (let c of this.commands) {
            nc.push(c);
        }
        for (let d of this.detail) {
            nd.push(d);
        }
        let nt = this.tagids.Clone();
        return new Record(this.id, nc, nd, nt);
    };
};


var RecordArray = function() {
    this.array = [];

    this.GetIndexById = function(id) {
        return find_if(this.array, function(e) {
            return e.id == id;
        });
    };

    this.GetRecordById = function(id) {
        let index = this.GetIndexById(id);
        if (index < 0) return null;
        return this.array[index];
    };

    this.AddAt = function(newrecord, index) {
        let record = this.GetRecordById(newrecord.id);
        if (record == null) {
            newrecord.id = AllocateId(this.array);
            this.array.splice(index, 0, newrecord);
            return newrecord.id;
        } else {
            record.Update(newrecord);
            return record.id;
        }
    };

    this.Add = function(newrecord) {
        this.AddAt(newrecord, this.array.length);
    };

    this.EraseById = function(id) {
        let index = this.GetIndexById(id);
        if (index < 0) return;
        this.array.splice(index, 1);
    };

    this.EraseByTagId = function(tagid) {
        let cont = false;
        do {
            cont = false;
            for (let i in this.array) {
                if (this.array[i].ContainTag(tagid)) {
                    this.array.splice(i, 1);
                    cont = true;
                    break;
                }
            }
        } while (cont == true);
    };

    this.Import = function(records) {
        this.array = [];
        for (let record of records) {
            let tagids = new UniqueIntArray();
            tagids.Import(record.tagids);
            this.array.push(new Record(record.id, record.commands, record.detail, tagids));
        }
    };

    this.Export = function() {
        let raw = [];
        for (let record of this.array) {
            raw.push(record.Export());
        }
        return raw;
    };

    this.Clone = function() {
        let n = new RecordArray();
        for (let record of this.array) {
            n.array.push(record.Clone());
        }
        return n;
    };
};


var Page = function(mtagid) {
    this.mtagid = mtagid;
    this.tags = new TagArray();
    this.records = new RecordArray();

    this.Update = function(other) {
        this.tags = other.tags;
        this.records = other.records;
    };

    this.EraseTag = function(tagid) {
        this.records.EraseByTagId(tagid);
        this.tags.EraseById(tagid);
    };

    this.Import = function(page) {
        this.mtagid = page.mtagid;
        this.tags.Import(page.tags);
        this.records.Import(page.records);
    };

    this.Export = function() {
        return {
            "mtagid": this.mtagid,
            "tags": this.tags.Export(),
            "records": this.records.Export()
        };
    };

    this.Clone = function() {
        let n = new Page(this.mtagid);
        n.tags = this.tags.Clone();
        n.records = this.records.Clone();
        return n;
    };
};


var PageCollection = function() {
    this.array = [];

    this.GetIndexById = function(mtagid) {
        return find_if(this.array, function(e) {
            return e.mtagid == mtagid;
        });
    };

    this.GetPageById = function(mtagid) {
        let index = this.GetIndexById(mtagid);
        if (index < 0) return null;
        return this.array[index];
    };

    this.Add = function(npage) {
        let page = this.GetPageById(npage.mtagid);
        if (page == null) {
            this.array.push(npage);
            return npage.mtagid;
        } else {
            page.Update(npage);
            return page.mtagid;
        }
    };

    this.EraseById = function(mtagid) {
        let index = this.GetIndexById(mtagid);
        if (index < 0) return;
        this.array.splice(index, 1);
    };

    this.Import = function(pages) {
        for (let page of pages) {
            let npage = new Page();
            npage.Import(page);
            this.array.push(npage);
        }
    };

    this.Export = function() {
        let raw = [];
        for (let page of this.array) {
            raw.push(page.Export());
        }
        return raw;
    };

    this.Clone = function() {
        let n = new PageCollection();
        for (let page of this.array) {
            n.array.push(page.Clone());
        }
        return n;
    };
};


var FullData = function() {
    this.mastertags = new TagArray();
    this.pages = new PageCollection();

    this.AddMasterTag = function(newmtag) {
        let tag = this.mastertags.GetTagById(newmtag.id);
        if (tag == null) {
            let mtagid = this.mastertags.Add(newmtag);
            return this.pages.Add(new Page(mtagid));
        } else {
            tag.Update(newmtag);
            return tag.id;
        }
    };

    this.EraseMasterTagById = function(mtagid) {
        this.pages.EraseById(mtagid);
        this.mastertags.EraseById(mtagid);
    };

    this.Import = function(data) {
        this.mastertags.Import(data.mastertags);
        this.pages.Import(data.pages);
    };

    this.Export = function() {
        return {
            "mastertags": this.mastertags.Export(),
            "pages": this.pages.Export()
        };
    };

    this.Clone = function() {
        let n = new FullData();
        n.mastertags = this.mastertags.Clone();
        n.pages = this.pages.Clone();
        return n;
    };
};