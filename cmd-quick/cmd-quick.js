var app = angular.module('CmdQuick', ['ngAnimate', 'ui.bootstrap', 'ui.sortable']);
app.controller('CmdQuickController', function($scope, $http, $uibModal, $log) {
    $scope.index = 0;
    $scope.data = new FullData();

    $scope.ChangeMasterTag = function(mtagid) {
        for (let i in $scope.data.pages.array) {
            if ($scope.data.pages.array[i].mtagid == mtagid) {
                $scope.index = i;
                break;
            }
        }
        $scope.filterText = "";
        $scope.filterTags = new UniqueIntArray();
    };

    // 
    $scope.GetMasterTagBtnClass = function(mtagid) {
        if (mtagid == $scope.data.pages.array[$scope.index].mtagid) {
            return "list-group-item active";
        } else {
            return "list-group-item";
        }
    };

    // get color for record
    $scope.GetRecordClass = function(id) {
        let cls = ["slide list-group-item", "slide list-group-item list-group-item-success", "slide list-group-item list-group-item-info", "slide list-group-item list-group-item-warning", "slide list-group-item list-group-item-danger"];
        return cls[id % cls.length];
    };


    // filter data
    $scope.filterText = "";
    $scope.filterTags = new UniqueIntArray();
    $scope.AddFilterTag = function(tagid) {
        $scope.filterTags.Add(tagid);
    };

    $scope.RemoveFilterTag = function(tagid) {
        $scope.filterTags.Erase(tagid);
    };

    $scope.ToggleFilterTag = function(tagid) {
        if ($scope.filterTags.GetIndex(tagid) < 0) {
            $scope.AddFilterTag(tagid);
        } else {
            $scope.RemoveFilterTag(tagid);
        }
    };

    $scope.GetFilterTagBtnClass = function(tagid) {
        if ($scope.filterTags.GetIndex(tagid) < 0) {
            return "btn btn-primary"; 
        } else {
            return "btn btn-warning";
        }
    };

    $scope.RecordsFilter = function(record, index, records) {
        if ($scope.filterTags.Length() != 0) {
            // check if all filter tag ids are included in record.tagids
            if (record.tagids.Include($scope.filterTags) == false) {
                return false;
            }
        }

        if ($scope.filterText.length >= 3) {
            let words = new TagArray();
            words.FromText($scope.filterText);

            let tags = new TagArray();
            tags.FromText(record.ToText());

            return tags.Include(words);
        } else {
            return true;
        }
    };

    $scope.TagsRetrieveById = function(tagid) {
        return $scope.data.pages.array[$scope.index].tags.GetTagById(tagid);
    };

    $scope.ShowRecordDialog = function(record) {
        let modalInstance = $uibModal.open({
            templateUrl: 'record-editor.html',
            controller: 'RecordEditorController',
            resolve: {
                record: function() { return record; },
                tags  : function() { return $scope.data.pages.array[$scope.index].tags; }
            }
        });

        modalInstance.result.then(function(record) {
            // save the record
            $scope.data.pages.array[$scope.index].records.Add(record);
            $scope.UpdateExportLink($scope.data, "ExportLink");
        }, function() {});
    }

    $scope.ShowNewRecordDialog = function() {
        $scope.ShowRecordDialog(new Record(-1, [], [], new UniqueIntArray()));
    };

    $scope.ShowNewRecordAtDialog = function(index) {
        let record = new Record(-1, [], [], new UniqueIntArray());
        let modalInstance = $uibModal.open({
            templateUrl: 'record-editor.html',
            controller: 'RecordEditorController',
            resolve: {
                record: function() { return record; },
                tags  : function() { return $scope.data.pages.array[$scope.index].tags; }
            }
        });

        modalInstance.result.then(function(record) {
            // save the record
            $scope.data.pages.array[$scope.index].records.AddAt(record, index);
            $scope.UpdateExportLink($scope.data, "ExportLink");
        }, function() {});
    };

    $scope.ShowEditRecordDialog = function(id) {
        let record = $scope.data.pages.array[$scope.index].records.GetRecordById(id);
        if (record == null) return;
        $scope.ShowRecordDialog(record.Clone());
    };

    $scope.ShowDeleteRecordDialog = function(id) {
        let modalInstance = $uibModal.open({
            templateUrl: 'confirm-dialog.html',
            controller: 'ConfirmDialogController',
            resolve: {
                title  : function() { return "Warning"; },
                message: function() { return "Are you sure?"; }
            }
        });

        modalInstance.result.then(function(status) {
            if (status == true) {
                $scope.data.pages.array[$scope.index].records.EraseById(id);
                $scope.UpdateExportLink($scope.data, "ExportLink");
            }
        }, function() {});
    };

    $scope.ShowManageTagsDialog = function() {
        let page = $scope.data.pages.array[$scope.index];
        let modalInstance = $uibModal.open({
            templateUrl: 'manage-tags.html',
            controller: 'ManageTagsController',
            size: 'sm',
            resolve: {
                tags: function() {
                    return page.tags.Clone();
                }
            }
        });
        modalInstance.result.then(function(newtags) {
            let cont = false;
            do {
                cont = false;
                for (let tag of page.tags.array) {
                    if (newtags.GetIndexById(tag.id) == -1) {
                        page.EraseTag(tag.id);
                        cont = true;
                        break;
                    }
                }
            } while (cont == true);
            page.tags = newtags;
            $scope.UpdateExportLink($scope.data, "ExportLink");
        }, function() {});
    };

    $scope.ShowManageMasterTagsDialog = function() {
        let modalInstance = $uibModal.open({
            templateUrl: 'manage-mastertags.html',
            controller: 'ManageMasterTagsController',
            size: 'sm',
            resolve: {
                mastertags: function() {
                    return $scope.data.mastertags.Clone();
                }
            }
        });
        modalInstance.result.then(function(mastertags) {
            // removing
            let cont = false;
            do {
                cont = false;
                for (let mtag of $scope.data.mastertags.array) {
                    if (mastertags.GetIndexById(mtag.id) == -1) {
                        $scope.data.EraseMasterTagById(mtag.id);
                        cont = true;
                        break;
                    }
                }
            } while (cont == true);

            // adding
            for (let mtag of mastertags.array) {
                if ($scope.data.mastertags.GetIndexById(mtag.id) == -1) {
                    $scope.data.AddMasterTag(mtag);
                }
            }

            // change order
            $scope.data.mastertags = mastertags;
            if ($scope.data.mastertags.array.length > 0) {
                $scope.ChangeMasterTag($scope.data.mastertags.array[0].id);
            }
            $scope.UpdateExportLink($scope.data, "ExportLink");
        }, function() {});
    };

    $scope.UpdateExportLink = function(fulldt, anchor) {
        var json = angular.toJson(fulldt.Export(), true);
        var blob = new Blob([json], {
            type: "application/json"
        });
        var url = URL.createObjectURL(blob);

        var a = document.getElementById(anchor);
        a.download = "cmd-quick.json";
        a.href = url;
    };

    let ReloadJson = function(json, reload) {
        let data = new FullData();
        data.Import(json);
        $scope.data = data;
        if ($scope.data.mastertags.array.length > 0) {
            $scope.ChangeMasterTag($scope.data.mastertags.array[0].id);
        }
        $scope.UpdateExportLink($scope.data, "ExportLink");
        if (reload) $scope.$apply();
    };

    // Import data frome chosen file
    $scope.Import = function(e) {
        let file = e.files[0];
        let reader = new FileReader();
        reader.onload = function() {
            ReloadJson(JSON.parse(reader.result), true);
        };
        reader.readAsText(file);
    };

    // Retrieve default data
    $http.get('cmd-quick.json').then(function(res) {
        ReloadJson(res.data, false);
    });


    $scope.sortableOptions = {
        axis: "y",
        opacity: 0.5,
        handle: ".reorder-handle",
        revert: 150,
        start: function(e, ui) {
            placeholderHeight = ui.item.outerHeight();
            ui.placeholder.height(placeholderHeight + 30);
            $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
        },
        change: function(e, ui) {
            ui.placeholder.stop().height(0).animate({
                height: ui.item.outerHeight() + 30
            }, 300);

            placeholderAnimatorHeight = parseInt($(".slide-placeholder-animator").attr("data-height"));

            $(".slide-placeholder-animator").stop().height(placeholderAnimatorHeight + 30).animate({
                height: 0
            }, 300, function() {
                $(this).remove();
                placeholderHeight = ui.item.outerHeight();
                $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
            });
        },
        stop: function(e, ui) {
            $(".slide-placeholder-animator").remove();
        }
    };
});


app.controller('RecordEditorController', function($scope, $uibModalInstance, record, tags) {
    $scope.id = record.id;
    $scope.cmdtxt = record.commands.join("\n");
    $scope.detailtxt = record.detail.join("\n");
    $scope.tagids = record.tagids;
    $scope.tags = tags;

    $scope.GetTagBtnClass = function(tagid) {
        if ($scope.tagids.GetIndex(tagid) >= 0) {
            return "btn btn-warning";
        } else {
            return "btn btn-primary";
        }
    };

    $scope.AddTagId = function(tagid) {
        $scope.tagids.Add(tagid);
    };
    $scope.RemoveTagId = function(tagid) {
        $scope.tagids.Erase(tagid);
    };

    $scope.ToggleTagId = function(tagid) {
        if ($scope.tagids.GetIndex(tagid) < 0) {
            $scope.AddTagId(tagid);
        } else {
            $scope.RemoveTagId(tagid);
        }
    };

    $scope.Save = function() {
        $uibModalInstance.close(new Record(
            $scope.id, $scope.cmdtxt.split("\n"),
            $scope.detailtxt.split("\n"), $scope.tagids
        ));
    };

    $scope.Cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});


app.controller('ConfirmDialogController', function($scope, $uibModalInstance, title, message) {
    $scope.title = title;
    $scope.message = message;

    $scope.Accept = function() {
        $uibModalInstance.close(true);
    };

    $scope.Reject = function() {
        $uibModalInstance.close(false);
    };
});


app.controller('ManageTagsController', function($scope, $uibModal, $uibModalInstance, tags) {
    $scope.tags = tags;
    $scope.name = "";

    $scope.AddTag = function() {
        let inform = function(title, message) {
            $uibModal.open({
                templateUrl: 'inform-dialog.html',
                controller: 'InformDialogController',
                resolve: {
                    title: function() { return title; },
                    message: function() { return message; }
                }
            });
        };
        if ($scope.name.length > 0) {
            $scope.tags.Add(new Tag(-1, $scope.name));
            $scope.name = "";
        } else {
            inform("Error", "Tag cannot have empty length.");
        }
    };

    $scope.RemoveTag = function(tagid) {
        let modalInstance = $uibModal.open({
            templateUrl: 'confirm-dialog.html',
            controller: 'ConfirmDialogController',
            resolve: {
                title: function() {
                    return "Warning";
                },
                message: function() {
                    return "Removing tag will also remove all records link to this tag!";
                }
            }
        });

        modalInstance.result.then(function(status) {
            if (status == true) {
                $scope.tags.EraseById(tagid);
            }
        }, function() {});
    };

    $scope.Save = function() {
        $uibModalInstance.close($scope.tags);
    };

    $scope.Cancel = function() {
       $uibModalInstance.dismiss('cancel');
    };

    $scope.sortableOptions = {
        opacity: 0.5,
        axis: 'y',
        placeholder: 'slide-placeholder',
        handle: ".reorder-handle",
        start: function(e, ui) {
            let placeholderHeight = ui.item.outerHeight();
            ui.placeholder.height(placeholderHeight + 15);
            $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
        },
        change: function(e, ui) {
            ui.placeholder.stop().height(0).animate({
                height: ui.item.outerHeight() + 15
            }, 300);
            let placeholderAnimatorHeight = parseInt($(".slide-placeholder-animator").attr("data-height"));
            $(".slide-placeholder-animator").stop().height(placeholderAnimatorHeight + 15).animate({
                height: 0
            }, 300, function() {
                $(this).remove();
                placeholderHeight = ui.item.outerHeight();
                $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
            });
        },
        stop: function(e, ui) {
            $(".slide-placeholder-animator").remove();
        }
    };
});


app.controller('ManageMasterTagsController', function($scope, $uibModal, $uibModalInstance, mastertags) {
    $scope.mastertags = mastertags;
    $scope.name = "";

    $scope.AddMasterTag = function() {
        let inform = function(title, message) {
            $uibModal.open({
                templateUrl: 'inform-dialog.html',
                controller: 'InformDialogController',
                resolve: {
                    title: function() { return title; },
                    message: function() { return message; }
                }
            });
        };
        if ($scope.name.length > 0) {
            if ($scope.mastertags.Add(new Tag(-1, $scope.name)) < 0) {
                inform("Inform", "You already have this category.");
            } else {
                $scope.name = "";
            }
        } else {
            inform("Error", "Category cannot have empty length.");
        }
    };

    $scope.RemoveMasterTag = function(tagid) {
        let modalInstance = $uibModal.open({
            templateUrl: 'confirm-dialog.html',
            controller: 'ConfirmDialogController',
            resolve: {
                title: function() {
                    return "Warning";
                },
                message: function() {
                    return "Removing category will also remove all records link to this category!";
                }
            }
        });

        modalInstance.result.then(function(status) {
            if (status == true) {
                $scope.mastertags.EraseById(tagid);
            }
        }, function() {});
    };

    $scope.Save = function() {
        $uibModalInstance.close($scope.mastertags);
    };

    $scope.Cancel = function() {
       $uibModalInstance.dismiss('cancel');
    };

    $scope.sortableOptions = {
        opacity: 0.5,
        placeholder: 'slide-placeholder',
        handle: ".reorder-handle",
        start: function(e, ui) {
            let placeholderHeight = ui.item.outerHeight();
            ui.placeholder.height(placeholderHeight + 10);
            $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
        },
        change: function(e, ui) {
            ui.placeholder.stop().height(0).animate({
                height: ui.item.outerHeight() + 10
            }, 300);
            let placeholderAnimatorHeight = parseInt($(".slide-placeholder-animator").attr("data-height"));
            $(".slide-placeholder-animator").stop().height(placeholderAnimatorHeight + 10).animate({
                height: 0
            }, 300, function() {
                $(this).remove();
                placeholderHeight = ui.item.outerHeight();
                $('<div class="slide-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
            });
        },
        stop: function(e, ui) {
            $(".slide-placeholder-animator").remove();
        }
    };
});


app.controller('InformDialogController', function($scope, $uibModalInstance, title, message) {
    $scope.title = title;
    $scope.message = message;

    $scope.Ok = function() {
        $uibModalInstance.dismiss('cancel');
    };
});